package com.example.giorgos.pebbleoflove;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.giorgos.pebbleoflove.tasks.Utils;
import com.getpebble.android.kit.PebbleKit;
import com.getpebble.android.kit.util.PebbleDictionary;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;


public class PebbleOfLove extends ActionBarActivity {

    public final static String URL_API_GET_MATCH = "http://192.168.10.35:3000/users/bob/match/?radius=0.5";
    public final static String URL_API_IGNORE = "http://192.168.10.35:3000/users/bob/ignore/john";
    public final static String URL_API_PUT_LOCATION = "http://192.168.10.35:3000/users/bob";
    private final static UUID PEBBLE_APP_UUID = UUID.fromString("08d083ac-8e11-4b2c-b5e8-2c77a3e913c4");
    public static TextView infoTextView;
    private static String state= "";
    private static String lastUser = "";

    public static Boolean active;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pebble_of_love);
        infoTextView = (TextView)findViewById(R.id.infoView);
        //TODO GET PEBBLE ON/OFF NOTIFICATION TO SET ACTIVE/PASSIVE
        active = PebbleKit.isWatchConnected(getApplicationContext());
        PebbleKit.startAppOnPebble(getApplicationContext(), PEBBLE_APP_UUID);
        PebbleKit.registerPebbleConnectedReceiver(getApplicationContext(), new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                active = PebbleKit.isWatchConnected(getApplicationContext());
                Log.i(getLocalClassName(), "Pebble connected!");
            }

        });

        PebbleKit.registerPebbleDisconnectedReceiver(getApplicationContext(), new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                active = PebbleKit.isWatchConnected(getApplicationContext());
                Log.i(getLocalClassName(), "Pebble disconnected!");
            }

        });


        PebbleKit.registerReceivedDataHandler(this, new PebbleKit.PebbleDataReceiver(PEBBLE_APP_UUID) {

            @Override
            public void receiveData(final Context context, final int transactionId, final PebbleDictionary data) {
                Log.i(getLocalClassName(), "Received value=" + data.getInteger(0) + " for key: 0");
                Boolean ignore = false;
                if(ignore)
                    new SendIgnoreTask().execute(URL_API_IGNORE);
//                PebbleKit.sendAckToPebble(getApplicationContext(), transactionId);
                PebbleDictionary response = new PebbleDictionary();

                response.addString(2, lastUser);



                PebbleKit.sendDataToPebble(getApplicationContext(), PEBBLE_APP_UUID, response);
            }


        });


        if(!active){
            // Launching my app
            PebbleKit.startAppOnPebble(getApplicationContext(), PEBBLE_APP_UUID);

        }
        // Launching my app
        PebbleKit.startAppOnPebble(getApplicationContext(), PEBBLE_APP_UUID);


        if (PebbleKit.areAppMessagesSupported(getApplicationContext())) {
            Log.i(getLocalClassName(), "App Message is supported!");
        } else {
            Log.i(getLocalClassName(), "App Message is not supported");
        }


        if(active){
            Timer timer = new Timer();

            timer.scheduleAtFixedRate(new TimerTask() {

                synchronized public void run() {
                    new SendLocationTask().execute(URL_API_PUT_LOCATION);

                }

            }, 30000, 30000);

            timer.scheduleAtFixedRate(new TimerTask() {

                synchronized public void run() {
                    new GetTask().execute(URL_API_GET_MATCH);
                }

            }, 5000, 60000);

        }


    }

    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            infoTextView.setText(state);

            try {
                JSONObject jObject = new JSONObject(state);
                PebbleKit.startAppOnPebble(getApplicationContext(), PEBBLE_APP_UUID);
                PebbleDictionary data = new PebbleDictionary();

                data.addString(1, jObject.getJSONArray("users").getJSONObject(0).get("username").toString());

                String previous = lastUser;

                lastUser =jObject.getJSONArray("users").getJSONObject(0).get("name").toString();
//                if(!lastUser.equals(previous)) {
                    data.addString(4, lastUser);
                    PebbleKit.sendDataToPebble(getApplicationContext(), PEBBLE_APP_UUID, data);
//                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    };





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pebble_of_love, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    class SendLocationTask  extends AsyncTask<String, String, String> {

        LocationManager mLocationManager;
        Location myLocation = getLastKnownLocation();


        @Override
        protected String doInBackground(String... params) {

            String urlString=params[0];

            double longitude = myLocation.getLongitude();
            double latitude = myLocation.getLatitude();

            InputStream inputStream = null;
            String result = "";

            try {
                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();
                // 2. make POST request to the given URL

                HttpPut httpPut = new
                        HttpPut(urlString);

                StringEntity se = new StringEntity("{\n" +
                        "  \"loc\" : {\n" +
                        "    \"coordinates\" : [\n" +
                        longitude +",\n" +
                        latitude+ "\n" +
                        "    ]\n" +
                        "  }\n" +
                        "}");
                // 6. set httpPost Entity
                httpPut.setEntity(se);
                // 7. Set some headers to inform server about the type of the content
                httpPut.addHeader("Accept", "application/json");
                httpPut.addHeader("Content-type", "application/json");
                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPut);


                //Try to add this
                inputStream = httpResponse.getEntity().getContent();

                if(inputStream != null)
                    result = Utils.convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                //Log.d("InputStream", e.getLocalizedMessage());
            }
            return result;

        }




        private Location getLastKnownLocation() {
            mLocationManager = (LocationManager)getApplicationContext().getSystemService(LOCATION_SERVICE);
            List<String> providers = mLocationManager.getProviders(true);
            Location bestLocation = null;
            for (String provider : providers) {
                Location l = mLocationManager.getLastKnownLocation(provider);
                if (l == null) {
                    continue;
                }
                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                    // Found best last known location: %s", l);
                    bestLocation = l;
                }
            }
            return bestLocation;
        }
    }

     class GetTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            String urlString=params[0]; // URL to call

            String resultToDisplay = "";

            // HTTP Get
            try {

                URL url = new URL(urlString);

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                resultToDisplay = Utils.convertInputStreamToString(urlConnection.getInputStream());

            } catch (Exception e ) {

                System.out.println(e.getMessage());

                return e.getMessage();

            }
            state = resultToDisplay;
            mHandler.obtainMessage(1).sendToTarget();
            return resultToDisplay;

        }

    }


    class SendIgnoreTask  extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... params) {

            String urlString = params[0];

            InputStream inputStream = null;
            String result = "";

            try {
                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();
                // 2. make POST request to the given URL

                HttpPut httpPut = new
                        HttpPut(urlString);

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPut);


                //Try to add this
                inputStream = httpResponse.getEntity().getContent();

                if (inputStream != null)
                    result = Utils.convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                //Log.d("InputStream", e.getLocalizedMessage());
            }
            return result;

        }
    }

    @Override
    public void onDestroy(){
        // Closing my app
//        PebbleKit.closeAppOnPebble(getApplicationContext(), PEBBLE_APP_UUID);
    }

}
