#include <pebble.h>

typedef struct {
    Window *window;
    TextLayer *found_layer;
    TextLayer *title_text;
    ActionBarLayer *action_bar;
    BitmapLayer *location_layer;

    GBitmap *up_bitmap;
    GBitmap *down_bitmap;
    GBitmap *location_bitmap;

    char name[100];
    char username[30];
} RequestUi;


static void answer_request(bool accept, char *username) {
    uint32_t key = accept ? 0x2 : 0x3;
    DictionaryIterator *iter;
    app_message_outbox_begin(&iter);

    if (iter == NULL) {
        APP_LOG(APP_LOG_LEVEL_DEBUG, "null iter");
        return;
    }

    Tuplet tuple = TupletCString(key, username);
    dict_write_tuplet(iter, &tuple);
    dict_write_end(iter);
    app_message_outbox_send();
}

static void accept_handler(void *recognizer, char *username) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Accepting %s", username);
    answer_request(true, username);
    window_stack_pop(true);
}

static void reject_handler(void *recognizer, char *username) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Rejecting %s", username);
    answer_request(false, username);
    window_stack_pop(true);
}

static void click_config_provider(void *context) {
    window_single_click_subscribe(
        BUTTON_ID_DOWN,
        (ClickHandler) reject_handler);
    window_single_click_subscribe(
        BUTTON_ID_UP,
        (ClickHandler) accept_handler);
}

static void load_request(Window *window) {
    Layer *window_layer = window_get_root_layer(window);
    RequestUi *ui = window_get_user_data(window);

    ui->found_layer = text_layer_create((GRect) {
        .origin = {0,0},
        .size = {124, 30}
    });
    text_layer_set_text(ui->found_layer, "Found");
    text_layer_set_text_alignment(ui->found_layer, GTextAlignmentCenter);
    text_layer_set_font(
        ui->found_layer,
        fonts_get_system_font(FONT_KEY_GOTHIC_24_BOLD)
    );
    layer_add_child(window_layer, text_layer_get_layer(ui->found_layer));

    ui->location_layer = bitmap_layer_create((GRect) {
        .origin = {35, 30},
        .size = {60, 75}
    });
    bitmap_layer_set_bitmap(ui->location_layer, ui->location_bitmap);
    Layer *layer = bitmap_layer_get_layer(ui->location_layer);
    layer_add_child(window_layer, layer);

    ui->title_text = text_layer_create((GRect) {
        .origin = {5,105},
        .size = {120,40}
    });
    text_layer_set_text_alignment(ui->title_text, GTextAlignmentCenter);
    text_layer_set_text(ui->title_text, ui->name);

    layer = text_layer_get_layer(ui->title_text);
    layer_add_child(window_layer, layer);

    // Action bar
    ui->action_bar = action_bar_layer_create();
    action_bar_layer_set_context(ui->action_bar, ui->username);
    action_bar_layer_add_to_window(ui->action_bar, ui->window);
    action_bar_layer_set_click_config_provider(
        ui->action_bar, click_config_provider);
    action_bar_layer_set_icon(ui->action_bar, BUTTON_ID_UP, ui->up_bitmap);
    action_bar_layer_set_icon(ui->action_bar, BUTTON_ID_DOWN, ui->down_bitmap);
}

static void unload_request(Window *window) {
    RequestUi *ui = window_get_user_data(window);

    text_layer_destroy(ui->title_text);
    text_layer_destroy(ui->found_layer);
    action_bar_layer_destroy(ui->action_bar);
    bitmap_layer_destroy(ui->location_layer);
    gbitmap_destroy(ui->up_bitmap);
    gbitmap_destroy(ui->down_bitmap);
    gbitmap_destroy(ui->location_bitmap);

    APP_LOG(APP_LOG_LEVEL_DEBUG, "Window unloaded");
    // This might not be the best place for free
    free(ui);
    window_destroy(window);
}

void create_request_window(char *_username, char *_name) {
    RequestUi *ui = malloc(sizeof(RequestUi));
    ui->window = window_create();
    window_set_user_data(ui->window, ui);
    window_set_window_handlers(ui->window, (WindowHandlers) {
        .load = load_request,
        .unload = unload_request
    });

    snprintf(ui->name, 100, "Interested in %s?", _name);
    strncpy(ui->username, _username, 30);

    ui->up_bitmap = gbitmap_create_with_resource(RESOURCE_ID_TICK);
    ui->down_bitmap = gbitmap_create_with_resource(RESOURCE_ID_CROSS);
    ui->location_bitmap = gbitmap_create_with_resource(RESOURCE_ID_LOCATION);

    window_stack_push(ui->window, true);
}
