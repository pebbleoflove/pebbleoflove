#include <pebble.h>
#include "request.h"
#include "accepted.h"

static Window *window;
static TextLayer *bluetooth_layer;
static GBitmap *splash_bitmap;
static BitmapLayer *splash_layer;

enum {
    // REQUEST, username
    PROPOSAL_REQUEST = 0x1,
    // ACCEPTED, username
    PROPOSAL_ACCEPTED = 0x2,
    // REJECTED, username
    PROPOSAL_REJECTED = 0x3,
    // Extra info for request
    USER_SCREEN_NAME = 0x4
};

void bt_handler(bool connected) {
    if(connected) {
        text_layer_set_text(bluetooth_layer, "connected");
    } else {
        text_layer_set_text(bluetooth_layer, "disconnected");
    }
}

static void outbox_failed_callback(
    DictionaryIterator *iterator,
    AppMessageResult reason,
    void *context
) {
  APP_LOG(APP_LOG_LEVEL_ERROR, "Outbox send failed!");
}

static void outbox_sent_callback(
    DictionaryIterator *iterator,
    void *context
) {
  APP_LOG(APP_LOG_LEVEL_INFO, "Outbox send success!");
}

static void window_load(Window *window) {
    Layer *window_layer = window_get_root_layer(window);
    GRect bounds = layer_get_bounds(window_layer);

    splash_layer = bitmap_layer_create((GRect) {
        .origin={0,0},
        .size={144,144}
    });
    bluetooth_layer = text_layer_create((GRect) { 
        .origin = {0, bounds.size.h - 20},
        .size = { bounds.size.w, 20 } 
    });

    text_layer_set_text_alignment(bluetooth_layer, GTextAlignmentCenter);
    text_layer_set_background_color(bluetooth_layer, GColorWhite);
    text_layer_set_text_color(bluetooth_layer, GColorBlack);

    bt_handler(bluetooth_connection_service_peek());
    bitmap_layer_set_bitmap(splash_layer, splash_bitmap);

    layer_add_child(window_layer, bitmap_layer_get_layer(splash_layer));
    layer_add_child(window_layer, text_layer_get_layer(bluetooth_layer));
}

static void window_unload(Window *window) {
    text_layer_destroy(bluetooth_layer);
    gbitmap_destroy(splash_bitmap);
    bitmap_layer_destroy(splash_layer);
}

static void accepted_vibe() {
    static const uint32_t const segments[] = { 200, 100, 400 };

    VibePattern pat = {
        .durations = segments,
        .num_segments = ARRAY_LENGTH(segments),
    };
    vibes_enqueue_custom_pattern(pat);
}

static void in_received_handler(DictionaryIterator *iter, void *context) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Message received");
    Tuple *request = dict_find(iter, PROPOSAL_REQUEST);
    Tuple *accepted = dict_find(iter, PROPOSAL_ACCEPTED);
    Tuple *name = dict_find(iter, USER_SCREEN_NAME);

    if(request) {
        APP_LOG(APP_LOG_LEVEL_DEBUG, "Date request: %s", request->value->cstring);
        vibes_long_pulse();
        char *username = request->value->cstring;
        create_request_window(username, name->value->cstring);
    }

    if(accepted) {
        APP_LOG(APP_LOG_LEVEL_DEBUG, "Accepted");
        accepted_vibe();
        create_accepted_window(accepted->value->cstring);
    }

}


static void init(void) {
    splash_bitmap = gbitmap_create_with_resource(RESOURCE_ID_SPLASH);

    window = window_create();
    window_set_window_handlers(window, (WindowHandlers) {
        .load = window_load,
        .unload = window_unload,
    });

    app_message_register_inbox_received(in_received_handler);
    app_message_register_outbox_failed(outbox_failed_callback);
    app_message_register_outbox_sent(outbox_sent_callback);
    app_message_open(128, 64);

    // on error.... potato
  const bool animated = true;
  window_stack_push(window, animated);
  bluetooth_connection_service_subscribe(bt_handler);
}

static void deinit(void) {
  window_destroy(window);
}

int main(void) {
  init();

  APP_LOG(APP_LOG_LEVEL_DEBUG, "Done initializing, pushed window: %p", window);

  app_event_loop();
  deinit();
}
