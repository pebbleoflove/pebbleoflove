'use strict';

var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var mongo = require('mongodb');
var dotenv = require('dotenv');
var _ = require('lodash');
var gravatar = require('gravatar');
var async = require('async');
var utils = require('./utils');

dotenv.load();

var MONGO_URL = process.env.MONGO_URL || 'mongodb://localhost:27017/pebbleoflove';

var app = express();
app.use(cors());
var jsonParser = bodyParser.json();

/**
 * Ping pong.
 */
app.get('/ping', function(req, res) {
  res.send('pong');
});

/**
 * Get list of all users.
 */
app.get('/users', function (req, res, next) {
  var db = req.app.get('db');
  var usersColl = db.collection('users');
  usersColl.find({}).toArray(function(err, users) {
    if (err) {
      return next(err);
    }
    console.log('Getting list of all users.');
    res.send({'users': users});
  });
});

/**
 * Get user details.
 */
app.get('/users/:username', function (req, res, next) {
  var username = req.params.username;
  var db = req.app.get('db');
  var usersColl = db.collection('users');
  usersColl.findOne({username: username}, function(err, user) {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.status(404).send({error: 'User not found.'});
    }
    console.log('Getting details for user %s.', username);
    res.send(user);
  });
});

/**
 * Get matches for user.
 */
app.get('/users/:username/match', function (req, res, next) {
  var radius = Number(req.query.radius);
  if (!radius) {
    return res.status(400).send({error: 'Radius not provided.'});
  }
  var username = req.params.username;
  var db = req.app.get('db');
  var usersColl = db.collection('users');
  usersColl.findOne({'username': username}, function(err, user) {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.status(404).send({error: 'User not found.'});
    }
    var query = {
      loc: {
        $geoWithin: {
          $center: [user.loc.coordinates, radius]
        }
      },
      username: {
        $ne: user.username
      }
    };
    usersColl.find(query).toArray(function(error, users) {
      if (error) {
        return next(err);
      }
      var withoutIgnored = utils.filterIgnored(user, users);
      var resultsWithScores = utils.addInterestsScores(user, withoutIgnored);
      var resultsSorted = _.sortByOrder(resultsWithScores, 'interestsScore', false);
      console.log('Getting matches for %s.', username);
      res.send({'users': resultsSorted});
    });
  });
});

/**
 * Update a users location.
 */

app.put('/users/:username', jsonParser, function (req, res, next) {
  if (!req.body) {
    return res.sendStatus(400);
  }
  var username = req.params.username;
  var db = req.app.get('db');
  var usersColl = db.collection('users');
  var body = req.body;
  usersColl.update({'username': username},
                   {$set: {'loc.coordinates': body.loc.coordinates}},
                   function(err, result) {
    if (err) {
      console.error('Failed to update user : ', username);
      return next(err);
    }
    console.log('Updated user : %s.', username);
    res.send(result);
  });
});

/**
 * Create a new user.
 */
app.post('/users', jsonParser, function (req, res, next) {
  if (!req.body) {
    return res.sendStatus(400);
  }
  var db = req.app.get('db');
  var usersColl = db.collection('users');
  var body = req.body;
  usersColl.insert(body, function(err, result) {
    if (err) {
      console.error('Failed to create new user.');
      return next(err);
    }
    console.log('Created new user.');
    res.send(result);
  });
});

/**
 * Ignore a user.
 */
app.put('/users/:username/ignore/:ignoredUsername', function (req, res, next) {
  var username = req.params.username;
  var ignoredUsername = req.params.ignoredUsername;
  var db = req.app.get('db');
  var usersColl = db.collection('users');
  usersColl.update({'username': username}, {$addToSet: {ignored: ignoredUsername}}, function(err, result) {
    if (err) {
      console.error('Failed to ignore %s for user %s.', ignoredUsername, username);
      return next(err);
    }
    console.log('Ignored %s for user %s.', ignoredUsername, username);
    res.send(result);
  });
});

/**
 * Reset app.
 */
app.delete('/reset', function (req, res, next) {
  var db = req.app.get('db');
  var usersColl = db.collection('users');
  usersColl.remove({}, function(err) {
    if (err) {
      console.error('Failed to delete users collection.');
      return next(err);
    }
    usersColl.insert(utils.initialUserData(), function(error, insertResult) {
      if (err) {
        console.log('Failed to reset app.');
        return next(err);
      }
      console.log('App had been reset.');
      res.send(insertResult);
    });
  });
});

app.get('/update-gravatars', function(req, res, next) {
  var db = req.app.get('db');
  var usersColl = db.collection('users');
  usersColl.find({}).toArray(function(err, users) {
    if (err) {
      next(err);
    }
    async.each(users, function(user, cb) {
      console.log('Fetching gravatar for user %s.', user.username);
      var gravatarOptions = {
        size: '144',
        default: 'retro',
        forcedefault: 'y'
      };
      var gravatarUrl = gravatar.url(user.email, gravatarOptions);
      console.log(gravatarUrl);
      usersColl.update({'_id': user._id}, {$set: {gravatar: gravatarUrl}}, function(error) {
        if (error) {
          console.log('Failed to update gravatar for user %s.', user.username);
        }
        cb();
      });
    }, function(error) {
      if (error) {
        var errMsg = 'Failed to update some avatars.';
        console.log(errMsg);
        return res.send({msg: errMsg});
      }
      var successMsg = 'Gravatars updated for all users.';
      console.log(successMsg);
      res.send({msg: successMsg});
    });
  });
});

app.put('/users/:user1/likes/:user2', function(req, res, next) {
  var user1 = req.params.user1;
  var user2 = req.params.user2;
  var users = _.sortBy([user1, user2]);
  var db = req.app.get('db');
  var matchesColl = db.collection('matches');
  var query = {
    user1: users[0],
    user2: users[1]
  };
  var update = {
    $set: {
      'user1': users[0],
      'user2': users[1]
    }
  };
  if (user1 === users[0]) {
    update.$set.likes = true;
  } else {
    update.$set.isLiked = true;
  }
  var options = {
    upsert: true
  };
  matchesColl.update(query, update, options, function(err, result) {
    if (err) {
      console.error('Failed to create new match.');
      return next(err);
    }
    console.log('Created new match.');
    res.send(result);
  });
});

app.get('/users/:username/yes', function(req, res, next) {
  var username = req.params.username;
  var db = req.app.get('db');
  var matchesColl = db.collection('matches');
  var query = {
    $or: [
      {user1: username},
      {user2: username}
    ],
    likes: true,
    isLiked: true
  };
  matchesColl.find(query).toArray(function(err, results) {
    if (err) {
      return next(err);
    }
    var usernames = [];
    _.each(results, function(item) {
      usernames.push(item.user1);
      usernames.push(item.user2);
    });
    var finalUsernames = _.uniq(usernames);
    var index = finalUsernames.indexOf(username);
    finalUsernames.splice(index, 1);
    res.send({users: finalUsernames});
  });
});

/**
 * Start server.
 */
mongo.MongoClient.connect(MONGO_URL, function(err, db) {
  if (err) {
    throw err;
  }
  app.set('db', db);
  console.log('Connected correctly to MongoDB.');
  var server = app.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log('App listening at http://%s:%s.', host, port);
  });
});
